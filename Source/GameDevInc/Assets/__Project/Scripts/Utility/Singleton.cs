﻿namespace GameDevInc
{
    /// <summary>
    /// A generic singleton.
    /// 
    /// Only one can ever exist at a time, and it will automatically be created when called if none exists.
    /// </summary>
    /// <typeparam name="T">The class that is inheriting this class.</typeparam>
    public abstract class Singleton<T> where T : class, new()
    {
        private static T _instance;
        private static object _lock = new object();

        /// <summary>
        /// The singular instance of this object.
        /// 
        /// Simply call this like you would any other class, using "SingletonName.instance".
        /// If an instance does not exist, it will be created before it is used.
        /// </summary>
        protected static T instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                        _instance = new T();

                    return _instance;
                }
            }
        }
    }
}