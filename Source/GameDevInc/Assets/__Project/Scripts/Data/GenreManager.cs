﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GameDevInc
{
    public class GenreManager : Singleton<GenreManager>
    {
        public static int genreCount
        {
            get
            {
                return instance.genres.Count;
            }
        }

        private Dictionary<string, Genre> genres = new Dictionary<string, Genre>();

        public static void LoadGenres()
        {
            instance.genres.Clear();

            instance.AddGenre(new Genre("action", "Action"));
            instance.AddGenre(new Genre("adventure", "Adventure"));
            instance.AddGenre(new Genre("rpg", "RPG"));
            instance.AddGenre(new Genre("simulation", "Simulation"));
            instance.AddGenre(new Genre("strategy", "Strategy"));
            instance.AddGenre(new Genre("casual", "Casual"));
        }

        public static Genre GetGenre(string guid)
        {
            if (!instance.genres.ContainsKey(guid))
                return null;

            return instance.genres[guid];
        }

        public static string[] GetAllGenreGuids()
        {
            return instance.genres.Keys.ToArray();
        }

        public static Genre[] GetAllGenres()
        {
            return instance.genres.Values.ToArray();
        }

        private void AddGenre(Genre genre)
        {
            if (genre == null)
            {
                Debug.LogError("Genre is null");
                return;
            }

            if (string.IsNullOrEmpty(genre.guid))
            {
                Debug.LogError("Genre guid is empty or null");
                return;
            }

            if (genres.ContainsKey(genre.guid))
            {
                Debug.LogError($"A Genre with guid '{genre.guid}' already exists");
                return;
            }

            genres.Add(genre.guid, genre);
        }
    }
}