﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameDevInc
{
    public class Game : Project
    {
        private Dictionary<string, float> genreWeight = new Dictionary<string, float>();
        private Dictionary<string, float> genreInfluence = new Dictionary<string, float>();
        private float totalWeight = 0.0f;
        private bool influenceCalculated = false;

        public float GetGenreWeight(string genreGuid)
        {
            if (!genreWeight.ContainsKey(genreGuid))
                return 0.0f;

            return genreWeight[genreGuid];
        }

        public float GetGenreWeight(Genre genre)
        {
            if (genre == null)
            {
                Debug.LogError("Genre is null");
                return -1.0f;
            }

            return GetGenreWeight(genre.guid);
        }

        public void SetGenreWeight(string genreGuid, float weight)
        {
            if (weight > 0.0f)
            {
                if (weight > 1.0f)
                    weight = 1.0f;

                if (genreWeight.ContainsKey(genreGuid))
                    totalWeight -= genreWeight[genreGuid];

                totalWeight += weight;
                genreWeight[genreGuid] = weight;
                influenceCalculated = false;
            }
            else
            {
                if (genreWeight.ContainsKey(genreGuid))
                {
                    totalWeight -= genreWeight[genreGuid];
                    genreWeight.Remove(genreGuid);
                    influenceCalculated = false;
                }
            }
        }

        public void SetGenreWeight(Genre genre, float weight)
        {
            if (genre == null)
            {
                Debug.LogError("Genre is null");
                return;
            }

            SetGenreWeight(genre.guid, weight);
        }

        public float GetGenreInfluence(string genreGuid)
        {
            if (!influenceCalculated)
            {
                Debug.LogError("Influence has not been calculated");
                return -1.0f;
            }

            if (!genreInfluence.ContainsKey(genreGuid))
                return 0.0f;

            return genreInfluence[genreGuid];
        }

        public float GetGenreInfluence(Genre genre)
        {
            if (genre == null)
            {
                Debug.LogError("Genre is null");
                return -1.0f;
            }

            return GetGenreInfluence(genre.guid);
        }

        public string[] GetGenreGuids()
        {
            return genreWeight.Keys.ToArray();
        }

        public Genre[] GetGenres()
        {
            string[] genreGuids = GetGenreGuids();
            Genre[] genres = new Genre[genreGuids.Length];

            for (int i = 0; i < genres.Length; i += 1)
                genres[i] = GenreManager.GetGenre(genreGuids[i]);

            return genres;
        }

        public void CalculateGenreInfluence()
        {
            genreInfluence.Clear();

            foreach (KeyValuePair<string, float> entry in genreWeight)
                genreInfluence[entry.Key] = entry.Value / totalWeight;

            influenceCalculated = true;
        }
    }
}