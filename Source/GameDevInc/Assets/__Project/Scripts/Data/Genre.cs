﻿namespace GameDevInc
{
    public class Genre
    {
        public string guid { get; private set; }
        public string name { get; private set; }

        public Genre(string guid, string name)
        {
            this.guid = guid;
            this.name = name;
        }
    }
}