﻿namespace GameDevInc
{
    public class Topic
    {
        public string guid { get; private set; }
        public string name { get; private set; }
        private float popularity = 0.0f;

        public Topic(string guid, string name)
        {
            this.guid = guid;
            this.name = name;
        }

        public float GetPopularity()
        {
            return popularity;
        }

        public void SetPopularity(float popularity)
        {
            if (popularity > 1.0f)
                popularity = 1.0f;
            else if (popularity < 0.0f)
                popularity = 0.0f;

            this.popularity = popularity;
        }
    }
}