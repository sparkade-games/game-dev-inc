﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GameDevInc
{
    public class TopicManager : Singleton<TopicManager>
    {
        public static int topicCount
        {
            get
            {
                return instance.topics.Count;
            }
        }

        private Dictionary<string, Topic> topics = new Dictionary<string, Topic>();

        public static void LoadTopics()
        {
            instance.topics.Clear();

            instance.AddTopic(new Topic("fantasy", "Fantasy"));
            instance.AddTopic(new Topic("scifi", "Science Fiction"));
            instance.AddTopic(new Topic("robots", "Robots"));
            instance.AddTopic(new Topic("wizards", "Wizards"));
            instance.AddTopic(new Topic("detectives", "Detectives"));
        }

        public static Topic GetTopic(string guid)
        {
            if (!instance.topics.ContainsKey(guid))
                return null;

            return instance.topics[guid];
        }

        public static string[] GetAllTopicGuids()
        {
            return instance.topics.Keys.ToArray();
        }

        public static Topic[] GetAllTopics()
        {
            return instance.topics.Values.ToArray();
        }

        private void AddTopic(Topic topic)
        {
            if (topic == null)
            {
                Debug.LogError("Topic is null");
                return;
            }

            if (string.IsNullOrEmpty(topic.guid))
            {
                Debug.LogError("Topic guid is empty or null");
                return;
            }

            if (topics.ContainsKey(topic.guid))
            {
                Debug.LogError($"A Topic with guid '{topic.guid}' already exists");
                return;
            }

            topics.Add(topic.guid, topic);
        }
    }
}