﻿using UnityEngine;

namespace GameDevInc
{
    public class GameManager : MonoBehaviour
    {
        private void Start()
        {
            GenreManager.LoadGenres();

            Game game = new Game();
            game.SetGenreWeight("action", 1.0f);
            game.SetGenreWeight("adventure", 2.0f);
            game.SetGenreWeight("casual", -1.0f);
            game.SetGenreWeight("simulation", 0.5f);
            game.CalculateGenreInfluence();

            Genre[] genres = game.GetGenres();
            for (int i = 0; i < genres.Length; i+= 1)
                Debug.Log($"{genres[i].name}: {game.GetGenreInfluence(genres[i])}");
        }
    }
}