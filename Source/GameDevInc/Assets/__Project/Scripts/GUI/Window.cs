﻿using UnityEngine.UI;
using UnityEngine;

namespace GameDevInc
{
    [RequireComponent(typeof(RectTransform))]
    public abstract class Window : MonoBehaviour
    {
        public Vector2 position
        {
            get
            {
                return new Vector2(rt.rect.x, rt.rect.y);
            }
        }

        public Vector2 size
        {
            get
            {
                return new Vector2(width, height);
            }

            set
            {
                width = value.x;
                height = value.y;
            }
        }

        public float width
        {
            get
            {
                return rt.rect.width;
            }

            set
            {
                rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, value);
            }
        }

        public float height
        {
            get
            {
                return rt.rect.height;
            }

            set
            {
                rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value);
            }
        }

        [SerializeField] private Button closeButton;

        private RectTransform rt
        {
            get
            {
                if (_rt == null)
                    _rt = GetComponent<RectTransform>();

                return _rt;
            }
        }
        private RectTransform _rt;

        protected virtual void OnOpen() { }
        protected virtual void OnClose() { }

        public void SetPosition(Vector2 pos)
        {
            pos = new Vector2(Mathf.Min(pos.x, Screen.width - width), Mathf.Min(pos.y, Screen.height - height));
            rt.position = pos;
        }

        public void Open()
        {
            OnOpen();
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
            OnClose();
        }
    }
}